# AWS-Policies-Elements

![picture alt](image.png "IAM")    

##### AWS IAM Policies Elements for References.
##### Version - 2012-10-17
###### JSON 

- IAM Policy [-iam]
- S3 Bucket Policy [-s3]
- Other 
  
**Reference: [https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html](https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html)**
